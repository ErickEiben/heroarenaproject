# README #

### What is this repository for? ###

* This Repository is going to be used for the Hero Arena Project for Advanced Game Workshop over the course of the semester.
* Version .1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Create a Bitbucket account [here](https://bitbucket.org/).
* Download Sourcetree and create an account [here](https://www.sourcetreeapp.com/).

* Do not make any commits until you are done all of your work and would like to upload your progress. Be careful messing with
* things that you do not know how to use.

### Contribution guidelines ###

* This is currently empty and is waiting to be filled out.